import 'dart:html';

import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Contact Form',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'Contact Form'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    final _formKey = GlobalKey<FormState>();
    return Scaffold(
      body: Form(
        key: _formKey,
        child: Container(
          decoration: BoxDecoration(gradient: LinearGradient(colors: [Colors.purple, Colors.lightBlue])),
          child: Padding(
            padding: const EdgeInsets.all(50.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Text("Contact Us", style: TextStyle(
                fontWeight: FontWeight.bold,
                  fontSize: 32.0
                  ),
                ),
                Text("Contact us directly if you have any questions"),
                SizedBox(height: 20.0),
                Column(
                  children: <Widget>[
                    TextFormField(
                      decoration:InputDecoration(
                        contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 15.0, 15.0),
                        labelText: "First Name",
                        hintText: "type your first name here",
                        filled: true,
                        border:
                        OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
                        validator: (value) {
                      if (value!.isEmpty) {
                        return 'Please eneter your first name';
                      }
                      return null;
                    },
                    ),
                    SizedBox(height: 16.0),
                    TextFormField(
                      decoration:InputDecoration(
                        contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 15.0, 15.0),
                        labelText: "Last Name",
                        hintText: "type your last name here",
                        filled: true,
                        border:
                        OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
                        validator: (value) {
                      if (value!.isEmpty) {
                        return 'Please enter your last name';
                      }
                      return null;
                    },
                    ),
                    SizedBox(height: 16.0),
                    TextFormField(
                      decoration:InputDecoration(
                        contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 15.0, 15.0),
                        labelText: "E-mail",
                        hintText: "Enter your active email address here",
                        filled: true,
                        border:
                        OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
                        validator: (value) {
                      if (value!.isEmpty) {
                        return 'Please Input your e-mail so we can contact you';
                      }
                      return null;
                    },
                    ),
                    SizedBox(height: 16.0),
                    TextFormField(
                      maxLength: 3000,
                      maxLines: 10,
                      decoration:InputDecoration(
                        contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 15.0, 15.0),
                        labelText: "Message",
                        hintText: "Please input your message here",
                        filled: true,
                        border:
                        OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
                        validator: (value) {
                      if (value!.isEmpty) {
                        return 'Message can not be empty!';
                      }
                      return null;
                    },
                    ),
                    SizedBox(height: 10.0),
                    MaterialButton(
                      height: 30,
                      color: Colors.black,
                      minWidth: double.infinity,
                      onPressed: (){
                        if(_formKey.currentState!.validate()){}
                      },
                      child: Text(
                        "SUBMIT",
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Colors.white,
                        ),
                      ),
                      ),
                  ],
                ),
              ],
            )
          ),
        )       // This trailing comma makes auto-formatting nicer for build methods.
      )
    );
  }
}
