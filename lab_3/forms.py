from lab_1.models import Friend
from django import forms
class Form(forms.ModelForm):
    class Meta:
        model = Friend
        fields = ("__all__")
    
