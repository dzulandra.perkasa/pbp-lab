from django.http.response import HttpResponseRedirect
from django.shortcuts import render
from lab_1.models import Friend
from .forms import Form
from django.contrib.auth.decorators import login_required

# Create your views here.
@login_required (login_url = "/admin/login/")
def index(request):
    friends = Friend.objects.all().values() 
    response = {'friends': friends}
    return render(request, 'lab3_index.html', response)

@login_required (login_url = "/admin/login/")
def add_friends(request):
    form = Form(request.POST)
    if request.method == 'POST'and form.is_valid():
        form.save()
        return HttpResponseRedirect ('/lab-3')
    else:
        form = Form
    return render(request, 'lab3_form.html', {"form":form})