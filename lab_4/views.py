from django.http.response import HttpResponseRedirect
from django.shortcuts import render
from lab_2.models import Note
from .forms import NoteForm

# Create your views here.
def index(request):
    note = Note.objects.all().values()
    response = { "note" : note }
    return render (request, "lab4_index.html", response)

def add_note(request):
    form = NoteForm(request.POST)
    if request.method == 'POST'and form.is_valid():
        form.save()
        return HttpResponseRedirect ('/lab-4')
    else:
        form = NoteForm
    return render(request, 'lab4_form.html', {"form":form})

def note_list(request):
    note = Note.objects.all().values()
    response = { "note" : note }
    return render (request, "lab4_note_list.html", response)