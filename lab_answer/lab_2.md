1. apa perbedaan antara XML dan JSON?
    dan json memliki perbedaan dimana xml merupakan markup language dan JSON sebagai format yang ditulis dengan javascript
    XML dan JSON juga memilki perbedaan pada cara penulisannya dimana xml menggunakan tag dan json berbasis objek

2. apa perbedaan antara HTML dan XML?
    HTML dan XML memiliki fokus yang berbeda dimana HTML fokus kepada penyajian data dan xml berfokus pada transfer data,
    juga tag data pada xml tidak ditentukan sebelumnya atau bisa dibilang bebas selama ada tag penutupnya sedangkan pada HTML
    tag sudah ditentukan oleh html sebelumnya tapi tidak strict terhadap tag penutup.

    reference
    https://blogs.masterweb.com/perbedaan-xml-dan-html/
    https://id.strephonsays.com/json-and-vs-xml-11034